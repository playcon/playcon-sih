<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','HomeController@index')->name('home');
Route::get('/admin', function () {
    return view('adminhome');
});

Route::get('/', function () {
    return view('welcome');
});


Route::get('/1', function () {
    return view('demo');
});




Auth::routes();
Route::resource('admin','AdminController');
Route::resource('trainer','TrainerController');
Route::resource('departments','DepartmentsController');
Route::resource('trainee','TraineeController');
Route::get('/home', 'HomeController@index')->name('home');
