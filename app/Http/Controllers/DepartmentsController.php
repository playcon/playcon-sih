<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departments;

class DepartmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $de = Departments::orderBy('dept_id','asce')->paginate(10);
        return view('departments.index')->with('tr',$de);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create');
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'dept_id' => 'required',
            'dept_name' => 'required',
        ]);
        $ins = new Departments;
        $ins->dept_id = $request->input('dept_id');
        $ins->dept_name = $request->input('dept_name');
        $ins->save();
        return redirect('admin')->with('success','Department Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $de = Departments::find($id);
        return view('departments.update')->with('tr',$de);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'dept_id' => 'required',
            'dept_name' => 'required',
        ]);
        $ins = Departments::find($id);
        $ins->dept_id = $request->input('dept_id');
        $ins->dept_name = $request->input('dept_name');
        $ins->save();
        return redirect('departments')->with('success','Department Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tr = Departments::find($id);
        if(auth()->user()->role != 'admin'){
            return redirect('admin')->with('error','Unauthorized access!');
        }
        $tr->delete();
        return redirect('admin')->with('success','Post Deleted');
    }
}
