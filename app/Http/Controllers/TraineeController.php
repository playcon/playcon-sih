<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trainee;

class TraineeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trainee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'dept_id' => 'required',
            'trainee_id' => 'required',
            'quli' => 'required',
            'image' => 'required'
        ]);
        $ins = new Trainee;
        $ins->name = $request->input('name');
        $ins->dept_id = $request->input('dept_id');
        $ins->trainee_id = $request->input('trainee_id');
        $ins->qualification = $request->input('quli');
        $ins->img = $request->input('image');
        $ins->save();
        return redirect('trainer_home')->with('success','Trainer Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tr = Trainee::find($id);
        return view('trainer.update')->with('tr',$tr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tr = Trainee::find($id);
        if(auth()->user()->role != 'admin'){
            return redirect('admin')->with('error','Unauthorized access!');
        }
        $tr->delete();
        return redirect('admin')->with('success','Post Deleted');
    }
}
