<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trainer;

class TrainerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trainer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'dept_id' => 'required',
            'trainer_id' => 'required',
            'quli' => 'required',
            'image' => 'required'
        ]);
        $ins = new Trainer;
        $ins->name = $request->input('name');
        $ins->dept_id = $request->input('dept_id');
        $ins->trainer_id = $request->input('trainer_id');
        
        $ins->qualification = $request->input('quli');
        $ins->img = $request->input('image');
        
        $ins->save();
        return redirect('admin')->with('success','Trainer Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tr = Trainer::find($id);
        return view('trainer.update')->with('tr',$tr);
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'dept_id' => 'required',
            'trainer_id' => 'required',
            'quli' => 'required',
            'image' => 'required'
        ]);
        $ins = Trainer::find($id);
        $ins->name = $request->input('name');
        $ins->dept_id = $request->input('dept_id');
        $ins->trainer_id = $request->input('trainer_id');
        $ins->qualification = $request->input('quli');
        $ins->img = $request->input('image');
        $ins->save();
        return redirect('admin')->with('success','Trainer Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tr = Trainer::find($id);
        if(auth()->user()->role != 'admin'){
            return redirect('admin')->with('error','Unauthorized access!');
        }
        $tr->delete();
        return redirect('admin')->with('success','Post Deleted');

    }
}
