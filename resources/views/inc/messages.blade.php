@if(count($errors) > 0)
    <br><br>
    @foreach ($errors->all() as $item)
        <div class="alert alert-danger">
            <?php echo $errors ?>
        </div>
    @endforeach
@endif

@if (session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif