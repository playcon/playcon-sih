<section id="footer">
          <div class="container">
            <div class="row">
            <div class="col 10 offset:s1">
                <h5 class="white-text">About Us</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/who-we-are/">Who We Are</a></li>
                  <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/history-milestones/">History & Milestones</a></li>
                  <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/our-founder/">Our Founder</a></li>
                  <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/accoaldes/">Accolades</a></li>
                  <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/our-management-team/">Leadership Team</a></li>
                  <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/contact-us/">Contact Us</a></li>
                </ul>
              </div>
              <div class="col l6 offset:s1 center">
               <div class="abc">
                <h5 class="white-text">Our Businness</h5>
                <li type="none"><a class="grey-text text-lighten-3 " href="http://shanthabiotech.com/press-release-2017/">Product Portfolio</a></li>
                <li type="none"><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/our-businness/product-portfolio/">Manufacturing Capabilities</a></li>
                <li type="none"><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/our-businness/rd-scientific-platform/">R&D Scientific Platform</a></li>
                </div>
              </div>
              <div class="col 10 offset:s2">
                <h5 class="white-text">Media</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/press-release-2017/">Press Release</a></li>
                  <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/shantha-in-news-2016/">Shantha in News</a></li>
                  <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/media/publications-clinical-and-rd/">Publications (Clinical and R&D)</a></li>
                  <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/media/media-kit/">Media Kit</a></li>
                  <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/media/media-contact/">Media Contact</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col 10 offset:s1">
                <ul class="center">
                <img class="responsive image" src="Wallpaper/20.png" style="height:100px;width:200px">
                </ul>
              </div>
            </div>
          <div class="footer-copyright">
            <div class="container">
            © 2014 Copyright shanth
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </section>