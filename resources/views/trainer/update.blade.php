@extends('layouts.app')

@section('content')
@if(count($errors) > 0)
<div class="alert alert-danger">
    <?php echo $errors ?>
</div>
@endif

<h1>Add Trainer</h1> 
        {!! Form::open(['action' =>['TrainerController@update' , $tr->id] , 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('title', 'Trainer ID')}}
            {{Form::text('trainer_id',$tr->trainer_id,['class' => 'form-control','placeholder' => 'Trainer ID'])}}
        </div>

        <div class="form-group">
            {{Form::label('title', 'Department ID')}}
            {{Form::text('dept_id',$tr->dept_id,['class' => 'form-control','placeholder' => 'Department ID'])}}
        </div>

        <div class="form-group">
            {{Form::label('title', 'Name')}}
            {{Form::text('name',$tr->name,['class' => 'form-control','placeholder' => 'Name'])}}
        </div>

        <div class="form-group">
            {{Form::label('trainer_id', 'Qualification')}}
            {{Form::text('quli',$tr->qualification,['class' => 'form-control','placeholder' => 'Qualification'])}}
        </div>

        <div class="form-group">
            {{Form::label('trainer_id', 'Image')}}
            {{Form::text('image',$tr->img,['class' => 'form-control','placeholder' => 'Image'])}}
        </div>
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}

@endsection
