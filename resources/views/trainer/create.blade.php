@extends('layouts.app')

@section('content')

<div class="container">
@if(count($errors) > 0)
<div class="alert alert-danger">
    <?php echo $errors ?>
</div>
@endif
<h1>Add Trainer</h1> 
        {!! Form::open(['action' => 'TrainerController@store' , 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('title', 'Trainer ID')}}
            {{Form::text('trainer_id','',['class' => 'form-control','placeholder' => 'Trainer ID'])}}
           
        </div>
        
       
        <div class="form-group">
            {{Form::label('title', 'Department ID')}}
            {{Form::text('dept_id','',['class' => 'form-control','placeholder' => 'Department ID'])}}
        </div>

        <div class="form-group">
            {{Form::label('title', 'Name')}}
            {{Form::text('name','',['class' => 'form-control','placeholder' => 'Name'])}}
        </div>

        <div class="form-group">
            {{Form::label('trainer_id', 'Qualification')}}
            {{Form::text('quli','',['class' => 'form-control','placeholder' => 'Qualification'])}}
        </div>

        <div class="form-group">
            {{Form::label('trainer_id', 'Image')}}
            {{Form::text('image','',['class' => 'form-control','placeholder' => 'Image'])}}
        </div>

        {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}
</div>
        <!-- <div class="float-left"> <a href='/sih19/public/admin/adminHome' class="btn btn-primary">Go Back</a></div> -->
        <!-- <a href="adminHome">Go Back</a> -->
@endsection
