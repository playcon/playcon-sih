<head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
     <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"  media="screen,projection"/>
      <!--Let browser know website is optimized for mobile-->
      <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"/> -->
      <title>CEO Home page</title>
    </head>
<table>
<tr><td><div class="row">
            <div class="col s12 m7">
            <div class="card" style="width:250px">
                <div class="card-image">
                <img src="Wallpaper/3-D12d.jpg" style="height:100px">
                <span class="card-title">Department 1</span>
                </div>
                <div class="card-content">
                <h5>GMP</h5>
                <p>Good Manufacturing Practice is a part of Quality Assurance this ensures that products are consistently produced and controlled to the quality standards appropriate to their intended use as required by the marketing authorization or product specification.</p>
                </div>
                <div class="card-action">
                <a href="http://127.0.0.1:8000/5">This is a link</a>
                </div>
            </div>
            </div>
</td>
<td><div class="col s8">
            <div class="card" style="width:250px">
                <div class="card-image">
                <img src="Wallpaper/18.jpg" style="height:100px">
                <span class="card-title">Department 2</span>
                </div>
                <div class="card-content">
                <h5>GLP</h5>
                <p style="text-align:justify">The laboratory premises and equipment should meet the general and specific requirements for Quality Control areas.The personnel, premises, and equipment in the laboratories should be appropriate to the tasks imposed by manufacturing operations.</p>
                </div>
                <div class="card-action">
                <a href="http://127.0.0.1:8000/6">This is a link</a>
                </div>
            </div>
            </div>
        </div>
</td>

<td><div class="col s8">
            <div class="card" style="width:250px">
                <div class="card-image">
                <img src="Wallpaper/18.jpg" style="height:100px">
                <span class="card-title">Department 3</span>
                </div>
                <div class="card-content">
                <h5>Quality Control</h5>
                <p>Quality Control is concerned with sampling,specifications and testing as well as the organization,documentation and release of the product procedures.The product to be released for sale or supply should be carried out only after checking their quality.</p>
                </div>
                <div class="card-action">
                <a href="http://127.0.0.1:8000/7">This is a link</a>
                </div>
            </div>
            </div>
        </div>
</td></tr>

<tr><td><div class="row">
            <div class="col s12 m7">
            <div class="card" style="width:250px">
                <div class="card-image">
                <img src="Wallpaper/3-D12d.jpg" style="height:100px">
                <span class="card-title">Department 4</span>
                </div>
                <div class="card-content">
                <h5>Quality Assurance</h5>
                <p>Quality Assurance is a wide ranging concept which covers all matters individually or collectively that influence the quality of a product. It is the sum or total of the organized arrangements made with the object of ensuring the medicinal products are of the quality required for their intended use.</p>
                </div>
                <div class="card-action">
                <a href="http://127.0.0.1:8000/8">This is a link</a>
                </div>
            </div>
            </div>
</td>
<td><div class="col s8">
            <div class="card" style="width:250px">
                <div class="card-image">
                <img src="Wallpaper/18.jpg" style="height:100px">
                <span class="card-title">Department 5</span>
                </div>
                <div class="card-content">
                <h5>Marketing</h5>
                <p>I am a very simple card. I am good at containing small bits of information.
                I am convenient because I require little markup to use effectively.</p>
                </div>
                <div class="card-action">
                <a href="http://127.0.0.1:8000/9">This is a link</a>
                </div>
            </div>
            </div>
        </div>
</td>

<td><div class="col s8">
            <div class="card" style="width:250px">
                <div class="card-image">
                <img src="Wallpaper/18.jpg" style="height:100px">
                <span class="card-title">Department 6</span>
                </div>
                <div class="card-content">
                <h5>R & D</h5>
                <p>R&D can be defined as any project to resolve scientific or technological uncertainty aimed at achieving an advance in science or technology. Those who are working in R&D-Pharmacy are termed as scientists with greater responsibilities and sharp knowledge.</p>
                </div>
                <div class="card-action">
                <a href="http://127.0.0.1:8000/10">This is a link</a>
                </div>
            </div>
            </div>
        </div>
</td></tr>

</table>