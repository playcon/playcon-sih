<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.app')

@section('title', 'Departments')

@section('content')
    <h1 class="blue-text">Departments</h1>
    <a class="btn blue white-text" href={{ route('departments.create') }}>
        <i class="material-icons left">add</i>
        New
    </a>
    <table>
        <thead>
            <tr>
                <th>Ref #</th>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($departments as $department) { ?>
                <tr>
                    <td>{{ $department->id }}</td>
                    <td>{{ $department->name }}</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
@endsection