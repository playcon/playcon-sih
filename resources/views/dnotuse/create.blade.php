<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.app')

@section('title', 'Departments')

@section('content')
    <div class="card">        
        {{ Form::open(['route' => 'departments.store']) }}
        <div class="card-content">
            <span class="card-title">
                Add Detail                
            </span>
            <div class="row">
                <div class="col s12 l4">
                    <div class="input-field">
                        <input type='text' name="name" id="name" />
                        <label for="name">Department Name</label>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="card-action">
            {{ Form::submit('Add', ['class' => 'btn pink white-text']) }}
        </div>
        {{ Form::close() }}
    </div>
@endsection