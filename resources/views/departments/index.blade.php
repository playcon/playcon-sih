@extends('layouts.app')

@section('content')

<!-- <?php echo $errors ?> -->

<div class="container">
    <!-- <div class="row justify-content-center">
        <div class="col-md-8">
        
            <div class="card">
            
            <div class="card-header" align='center'><h3>Department Details</h3></div>
            <br/>
            <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        {{-- You are logged in as {{Auth::user()->name}}! --}}
                        {{-- DEPARTMENT TABLE HERE  --}}
                        <table class="table table-stripped">
                            <tr>
                                    <th>Dept ID</th>
                                    <th>Dept Name</th>
                            </tr>
                            
                        @foreach ($tr as $item)
                            <tr>                                    
                                    <td>{{$item->dept_id}}</td>
                                    <td>{{$item->dept_name}}</td>
                                    <td><a href='/sih19/public/departments/{{$item->id}}/edit' class="btn btn-success">Update</a></td>
                                    <td>{!!Form::open(['action' => ['DepartmentsController@destroy',$item->id], 'method' => 'post' ,'class' => 'pull-right'])!!}
                                        {{Form::hidden('_method','DELETE')}}
                                        {{Form::submit('Delete',['class'=>'btn btn-danger'])}}
                                        {!!Form::close()!!}
                                    </td>
                            </tr>

                        @endforeach                                      
                        </table>

                        <div class="float-left"> <a href='/sih19/public/departments/create' class="btn btn-primary">Add Departments</a></div>                   
                        <div class="float-right"> {{$tr->links()}} </div>          

                </div>
            </div>
        </div>
    </div> -->
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
     <!-- <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/> -->
      <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"  media="screen,projection"/>
      <!--Let browser know website is optimized for mobile-->
      <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"/> -->
      <title>CEO Home page</title>
    </head>
    <body>
    <div class="card-deck">
        <div class="row">
            <div class="card col-sm">
                <div style="margin-top:15px" >
                <img class="card-img-top" src="Wallpaper/1bg.jpg" alt="Card image cap">
                </div>
                <div class="card-body">
                    <h3 class="card-title">Department 1</h3>
                    <h5>GMP</h5>
                    <p class="card-text" style="text-align:justify">Good Manufacturing Practice is a part of Quality Assurance this ensures that products are consistently produced and controlled to the quality standards appropriate to their intended use as required by the marketing authorization or product specification.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>
            <div class="card col-sm">
                <div style="margin-top:15px" >
                <img class="card-img-top" src="Wallpaper/2bg.jpg" alt="Card image cap">
                </div>
                <div class="card-body">
                    <h3 class="card-title">Department 2</h3>
                    <h5>GLP</h5>
                    <p class="card-text"  style="text-align:justify">The laboratory premises and equipment should meet the general and specific requirements for Quality Control areas.The personnel, premises, and equipment in the laboratories should be appropriate to the tasks imposed by manufacturing operations.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>
            <div class="card col-sm">
                <div style="margin-top:15px" >
                <img class="card-img-top" src="Wallpaper/3bg.jpg" alt="Card image cap">
                </div>
                <div class="card-body">
                    <h3 class="card-title">Department 3</h3>
                    <h5>Quality Control</h5>
                    <p class="card-text"  style="text-align:justify">Quality Control is concerned with sampling,specifications and testing as well as the organization,documentation and release of the product procedures.The product to be released for sale or supply should be carried out only after checking their quality.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>
        </div>
        <div class="row"style="margin-top:50px">
            <div class="card col-sm">
                <div style="margin-top:15px" >
                <img class="card-img-top" src="Wallpaper/4bg.jpg" alt="Card image cap">
                </div>
                <div class="card-body">
                    <h3 class="card-title">Department 4</h3>
                    <h5>Quality Assurance</h5>
                    <p class="card-text"  style="text-align:justify">Quality Assurance is a wide ranging concept which covers all matters individually or collectively that influence the quality of a product. It is the sum or total of the organized arrangements made with the object of ensuring the medicinal products are of the quality required for their intended use.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>
            <div class="card col-sm">
                <div style="margin-top:15px" >
                <img class="card-img-top" src="Wallpaper/5bg.jpg" alt="Card image cap">
                </div>
                <div class="card-body">
                    <h3 class="card-title">Department 5</h3>
                    <h5>Marketing</h5>
                    <p class="card-text"  style="text-align:justify">I am a very simple card. I am good at containing small bits of information.I am convenient because I require little markup to use effectively.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>
            <div class="card col-sm">
                <div style="margin-top:15px" >
                <img class="card-img-top" src="Wallpaper/6bg.png" alt="Card image cap">
                </div>
                <div class="card-body">
                    <h3 class="card-title">Department 6</h3>
                    <h5>R & D</h5>
                    <p class="card-text"  style="text-align:justify">R&D can be defined as any project to resolve scientific or technological uncertainty aimed at achieving an advance in science or technology. Those who are working in R&D-Pharmacy are termed as scientists with greater responsibilities and sharp knowledge.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>
        </div>
    </div>
</div>
    <section id="footer" style="background-color:lightgreen">
		<div class="container"style="margin-top:20px">
			<div class="row text-center text-xs-center text-sm-left text-md-left">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5 style="margin-top:25px">About Us</h5>
					<ul class="list-unstyled quick-links" >
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/who-we-are/">Who We Are</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/history-milestones/">History & Milestones</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/our-founder/">Our Founder</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/accoaldes/">Accolades</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/our-management-team/">Leadership Team</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/contact-us/">Contact Us</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
				    <h5 style="margin-top:25px">Our Businness</h5>
					<ul class="list-unstyled quick-links">
                        <li type="none"><a class="grey-text text-lighten-3 " href="http://shanthabiotech.com/press-release-2017/">Product Portfolio</a></li>
                        <li type="none"><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/our-businness/product-portfolio/">Manufacturing Capabilities</a></li>
                        <li type="none"><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/our-businness/rd-scientific-platform/">R&D Scientific Platform</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5 style="margin-top:25px">Media</h5>
					<ul class="list-unstyled quick-links">
                        <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/press-release-2017/">Press Release</a></li>
                        <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/shantha-in-news-2016/">Shantha in News</a></li>
                        <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/media/publications-clinical-and-rd/">Publications (Clinical and R&D)</a></li>
                        <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/media/media-kit/">Media Kit</a></li>
                        <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/media/media-contact/">Media Contact</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
					<p><u><a href="http://shanthabiotech.com/copyright/">COPYRIGHT-Shantha Biotechnics Private Limited. </a><a href="http://shanthabiotech.com/legal-notice/" style="margin-left:30px">Legal Notice</a><a href="http://shanthabiotech.com/terms-of-use/" style="margin-left:30px">Terms Of Use</a><a href="http://shanthabiotech.com/sitemap/">Sitemap</a></p>
				</div>
				</hr>
			</div>	
		</div>
	</section>
    </body>

@endsection