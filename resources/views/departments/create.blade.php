@extends('layouts.app')

@section('content')

<!-- @if(count($errors) > 0)
<div class="alert alert-danger">
    <?php echo $errors ?>
</div>
@endif -->
<div class="container">
<h1>Add Departments</h1> 

        {!! Form::open(['action' => 'DepartmentsController@store' , 'method' => 'POST']) !!}
        <div class="form-group">
                    {{Form::label('title', 'Department ID')}}
                    {{Form::text('dept_id','',['class' => 'form-control','placeholder' => 'Department ID'])}}
                </div>

        <div class="form-group">
            {{Form::label('title', 'Department Name')}}
            {{Form::text('dept_name','',['class' => 'form-control','placeholder' => 'Department Name'])}}
        </div>

        {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}
</div>
@endsection
