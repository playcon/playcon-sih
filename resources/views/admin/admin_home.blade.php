@extends('layouts.app')

 @section('content')
{{-- ERRORS --}}
@include('inc.messages')




<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
     <!-- <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/> -->
      <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"  media="screen,projection"/>
      <!--Let browser know website is optimized for mobile-->
      <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"/> -->
      <title>CEO Home page</title>
    </head>
    
    <body>
    <div class="container">
        <ul class="center">
        <img class="img-circle rounded mx-auto d-block" src="Wallpaper/user-male-shape-in-a-circle-ios-7-interface-symbol_318-39025.jpg" style="height:200px" /><br/>
        </ul>
            <!-- <img class="img-circle" src="Wallpaper/nature20.jpg" style="height:200px"> -->
            <ul class="text-center">
            <div style="font-size: 150%">Jariwala vishal yogeshkumar</div>
            <div>CEO , Santha Sanofi</div>
            <br/><br/>
            </ul>
        <ul class="center">
        <div class="row">
        <div class="col-sm">
            <div class="card text-black" style="max-width: 18rem;background-color:lightgreen">
                <div class="card-header"style="font-size: 150%">About</div>
                <div class="card-body">
                    <p class="card-text">PHD in Pharmacy</p>
                    <p class="card-text">PHD in vaccine</p>
                </div>
            </div>
        </div>
        <div class="col-sm">
            <div class="card text-black" style="max-width: 18rem;background-color:lightgreen">
                <div class="card-header"style="font-size: 150%">News update</div>
                <div class="card-body">
                    <p class="card-text">PHD in Pharmacy</p>
                    <p class="card-text">PHD in vaccine</p>
                </div>
             </div>
        </div>
        <div class="col-sm">
            <div class="card text-black" style="max-width: 18rem;background-color:lightgreen">
                <div class="card-header"style="font-size: 150%">vaccine prouction</div>
                <div class="card-body">
                    <p class="card-text">SHAN_TT (IN STOCK)</p>
                    <p class="card-text">SHAN5 (OUT OF)</p>
                </div>
             </div>
        </div>
        </ul>
        <br/><br/><br/>
    </div> 
    <section id="footer" style="background-color:lightgreen">
		<div class="container"style="margin-top:20px">
			<div class="row text-center text-xs-center text-sm-left text-md-left">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5 style="margin-top:25px">About Us</h5>
					<ul class="list-unstyled quick-links" >
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/who-we-are/">Who We Are</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/history-milestones/">History & Milestones</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/our-founder/">Our Founder</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/accoaldes/">Accolades</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/our-management-team/">Leadership Team</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://shanthabiotech.com/about-us/contact-us/">Contact Us</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
				    <h5 style="margin-top:25px">Our Businness</h5>
					<ul class="list-unstyled quick-links">
                        <li type="none"><a class="grey-text text-lighten-3 " href="http://shanthabiotech.com/press-release-2017/">Product Portfolio</a></li>
                        <li type="none"><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/our-businness/product-portfolio/">Manufacturing Capabilities</a></li>
                        <li type="none"><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/our-businness/rd-scientific-platform/">R&D Scientific Platform</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5 style="margin-top:25px">Media</h5>
					<ul class="list-unstyled quick-links">
                        <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/press-release-2017/">Press Release</a></li>
                        <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/shantha-in-news-2016/">Shantha in News</a></li>
                        <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/media/publications-clinical-and-rd/">Publications (Clinical and R&D)</a></li>
                        <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/media/media-kit/">Media Kit</a></li>
                        <li><a class="grey-text text-lighten-3" href="http://shanthabiotech.com/media/media-contact/">Media Contact</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
					<p><u><a href="http://shanthabiotech.com/copyright/">COPYRIGHT-Shantha Biotechnics Private Limited. </a><a href="http://shanthabiotech.com/legal-notice/" style="margin-left:30px">Legal Notice</a><a href="http://shanthabiotech.com/terms-of-use/" style="margin-left:30px">Terms Of Use</a><a href="http://shanthabiotech.com/sitemap/">Sitemap</a></p>
				</div>
				</hr>
			</div>	
		</div>
	</section>
      <!--JavaScript at end of body for optimized loading-->
      <!-- <script type="text/javascript" src="js/materialize.min.js"></script> -->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- <script type="text/javascript" src="js/inite.js"></script> -->
    </body>
  </html>
  



@endsection