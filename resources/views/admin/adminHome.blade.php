<!-- Admin here!!!!! -->
 @extends('layouts.app')

 @section('content')
{{-- ERRORS --}}
@include('inc.messages')


 <div class="container">
 
    <div class="row justify-content-center">
        <div class="col-md-8">
        
            <div class="card">

            <!-- This is basic layout of Departments and trainer's dashboard -->
          
            
            <div class="card-header text-center"><h3 class="text-white bg-dark">Admin's Dashboard</h3></div>
                <div class="card-header"><h4>Trainers</h4></div>
                <br/>
                

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- You are logged in as {{Auth::user()->name}}! --}}
                     {{-- TRAINEE   TABLE HERE  --}}
                    <table class="table table-stripped">
                        <tr>
                            <th>Name</th>
                            <th>Dept ID</th>
                            <th>Trainer ID</th>
                            <th>Qualification</th>
                            <th>Image</th>
                        </tr>
                    @foreach ($tr as $item) 
                    <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->dept_id}}</td>
                        <td>{{$item->trainer_id}}</td>
                        <td>{{$item->qualification}}</td>
                        <td>{{$item->img}}</td>
                        <td><a href='#' class="btn btn-success">Show Details</a></td>
                        <td><a href='/sih19/public/trainer/{{$item->id}}/edit' class="btn btn-success">Update</a></td>
                        
                        <td>{!!Form::open(['action' => ['TrainerController@destroy',$item->id], 'method' => 'post' ,'class' => 'pull-right'])!!}
                            {{Form::hidden('_method','DELETE')}}
                            {{Form::submit('Delete',['class'=>'btn btn-danger'])}}
                            {!!Form::close()!!}
                        </td>
                        
                    </tr>                
                    @endforeach  
                                    
                    </table>                                                         
                    <div class="float-left"> <a href='/sih19/public/trainer/create' class="btn btn-primary">Add Trainer</a></div>                    
                    <div class="float-right"> {{$tr->links()}} </div>   

                   

                   
                      
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
