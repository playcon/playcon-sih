@extends('layouts.app')

@section('content')
<body background="Wallpaper\WhatsApp Image 2019-03-02 at 9.35.15 AM.jpeg" >

    <div class="container" style="width:400px">
    <h1 class="text-center ">Add Trainee</h1>
        {!! Form::open(['action' => 'TraineeController@store' , 'method' => 'POST']) !!}
        <div class="form-group required">
            {{Form::label('title', 'Trainee ID')}}
            {{Form::text('trainee_id','',['class' => 'form-control','placeholder' => 'Trainee ID','required'])}}
        </div>
       
        <div class="form-group required">
            {{Form::label('title', 'Department ID')}}
            {{Form::text('dept_id','',['class' => 'form-control','placeholder' => 'Department ID','required'])}}
        </div>

        <div class="form-group">
            {{Form::label('title', 'Name')}}
            {{Form::text('name','',['class' => 'form-control','placeholder' => 'Name','required'])}}
        </div>

        <div class="form-group">
            {{Form::label('trainee_id', 'Qualification')}}
            {{Form::text('quli','',['class' => 'form-control','placeholder' => 'Qualification','required'])}}
        </div>

        <div class="form-group">
            {{Form::label('trainee_id', 'Image')}}
            {{Form::text('image','',['class' => 'form-control','placeholder' => 'Image'])}}
        </div>

        {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}
        </table>
    </div>
</body>
        <!-- <div class="float-left"> <a href='/sih19/public/admin/adminHome' class="btn btn-primary">Go Back</a></div> -->
        <!-- <a href="adminHome">Go Back</a> -->
@endsection
