<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();

        Schema::create('trainers', function (Blueprint $table) {
            //$table->increments('id');
            $table->increments('trainer_id');
            $table->unsignedInteger('dept_id');
            //$table->foreign('dept_id')->references('dept_id')->on('department');
            $table->string('name');
            $table->string('qualification');
            $table->string('img');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainers');
    }
}
